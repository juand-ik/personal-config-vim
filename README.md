# Instalación

## OSX
**1. Activa modo desarrollador**

```
defaults write com.apple.appstore ShowDebugMenu -bool true
```

## Unix
**2. Ejecutar la siguiente línea( Crear una carpeta y descargar en pathogen )**

```
mkdir -p ~/.vim/autoload ~/.vim/bundle && curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
```

**3. Instala**

```
curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
```

## Plugins

**[EasyMotion:](https://github.com/easymotion/vim-easymotion)**

```
git clone https://github.com/easymotion/vim-easymotion ~/.vim/bundle/vim-easymotion
```

**[Javascript syntax:](https://github.com/pangloss/vim-javascript)**

```
git clone https://github.com/pangloss/vim-javascript.git ~/.vim/bundle/vim-javascript
```

**[Syntastic:](https://github.com/vim-syntastic/syntastic)**

```
git clone https://github.com/scrooloose/syntastic ~/.vim/bundle/syntastic
```

**[Emmet:](https://github.com/mattn/emmet-vim)**

```
git clone https://github.com/mattn/emmet-vim ~/.vim/bundle/emmet-vim
```

**[Css Colors:](https://github.com/ap/vim-css-color)**

```
git clone https://github.com/ap/vim-css-color ~/.vim/bundle/vim-css-color
```

**[Stylus syntax:](https://github.com/wavded/vim-stylus)**

```
git clone https://github.com/wavded/vim-stylus ~/.vim/bundle/vim-stylus
```

**[Nerdtree:](https://github.com/scrooloose/nerdtree.git)**

```
git clone https://github.com/scrooloose/nerdtree.git ~/.vim/bundle/nerdtree
```

**[Airlines:](https://github.com/bling/vim-airline)**

```
git clone https://github.com/bling/vim-airline ~/.vim/bundle/vim-airline
```

**[Molokai:](https://github.com/tomasr/molokai)**

```
git clone https://github.com/tomasr/molokai ~/.vim/bundle/molokai
```

**[Dracula:](https://github.com/dracula/vim)**

```
git clone https://github.com/dracula/vim ~/.vim/bundle/dracula
```

**[Iceberg](https://github.com/cocopon/iceberg.vim)**

```
git clone https://github.com/cocopon/iceberg.vim ~/.vim/bundle/iceberg
```

**[Vim Atom dark](https://github.com/gosukiwi/vim-atom-dark)**

```
git clone https://github.com/gosukiwi/vim-atom-dark ~/.vim/bundle/vim-atom-dark
```

**[Snazzy theme](https://github.com/connorholyday/vim-snazzy)**

```
git clone https://github.com/connorholyday/vim-snazzy.git ~/.vim/bundle/vim-snazzy
```

**[UltiSnips:](https://github.com/sirver/ultisnips)**

```
git clone https://github.com/sirver/ultisnips ~/.vim/bundle/ultisnips
```

**[TypeScript:](https://github.com/leafgarland/typescript-vim)**

```
git clone https://github.com/leafgarland/typescript-vim ~/.vim/bundle/typescript-vim
```

**[Devicons](https://github.com/ryanoasis/vim-devicons)**

```
git clone https://github.com/ryanoasis/vim-devicons ~/.vim/bundle/vim-devicons
```

**[Nerdtree-Syntax Highlight:](https://github.com/tiagofumo/vim-nerdtree-syntax-highlight)**

```
git clone https://github.com/tiagofumo/vim-nerdtree-syntax-highlight ~/.vim/bundle/vim-nerdtree-syntax-highlight
```

## Crear Archivo .vimrc ~ con vim :v

`vim ~/.vimrc`

* **Si nerdtree, no muestra subcarpetas de forma correcta, es probable que necesites setear a utf-8:**
```
set encoding=utf-8
```

copiar y pegar el contenido del archivo adjunto

## ZSH

### Instalación en `linux`(debian)

Abre la terminal y copia la siguiente linea:

```
sudo apt install zsh
```

## Instalación de Oh-My-ZSH



```
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
```

### Instala PowerLevel9k!

```
git clone https://github.com/bhilburn/powerlevel9k.git ~/.oh-my-zsh/custom/themes/powerlevel9k
```

Integra el tema:

Posteriormente editamos el archivo `~/.zshrc`

```
vim ~/.zshrc
```

1. Cambiamos el tema que viene por defecto `ZSH_THEME="robbyrussell` por el de PowerLevel:

   ```
   ...

   # to know which specific one was loaded, run: echo $RANDOM_THEME
   # See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes

   # ZSH_THEME="robbyrussell"
   ZSH_THEME="powerlevel9k/powerlevel9k"

   ...
   ```

   Requiere descargar e instalar la fuente [NerdFont](https://github.com/ryanoasis/nerd-fonts/blob/master/patched-fonts/Hack/Regular/complete/Hack%20Regular%20Nerd%20Font%20Complete.ttf)

2. Ubicamos `plugins=(git)` y en la linea siguiente pegamos lo siguiente:

   ```
   ...
   plugins=(git)

   # User configuration
   POWERLEVEL9K_MODE="nerdfont-complete"
   POWERLEVEL9K_DISABLE_RPROMPT=true
   POWERLEVEL9K_PROMPT_ON_NEWLINE=true
   POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(dir newline vcs)
   POWERLEVEL9K_COLOR_SCHEME='light'
   POWERLEVEL9K_VCS_BRANCH_ICON=$'\uF126 '
   POWERLEVEL9K_MULTILINE_LAST_PROMPT_PREFIX="→ ~ "
   POWERLEVEL9K_MULTILINE_FIRST_PROMPT_PREFIX=""

   ...
   ```

3. Casi a esta misma altura encontramos `source $ZSH/oh-my-zsh.sh`, damos enter para en una nueva linea pegar.
   ```
   prompt_context() {
   }
   ```

4. Guardamos y salimos del archivo.

5. Ahora hay que cambiar la consola por defecto(bash) por zsh, para esto abre la terminal y teclea:

   ```
   zsh
   ```

   * Estando dentro de zsh pegamos lo siguiente, para cambiar a tener zsh:

    ```
    chsh -s $(which zsh)
    ```

6. Para finalizar procedemos a hacer un logout o reinicio.

* Nota: no olvides editar el perfil de la terminal y agregar la fuente de NerdFont, previamente descargada.

## ZSH Pluggins

### Instalación:

**[fast-syntax-highlighting](https://github.com/zdharma-continuum/fast-syntax-highlighting)**
Permite resaltar los comandos existentes

```
git clone https://github.com/zdharma-continuum/fast-syntax-highlighting.git \
  ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/plugins/fast-syntax-highlighting
```

**[zsh-autosuggestions](https://github.com/zsh-users/zsh-autosuggestions/blob/master/INSTALL.md)**
Autocompleta o realiza sugerencias de los comandos usados en la terminal

```
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
```

**[copydir](https://github.com/zsh-users/zsh-autosuggestions/blob/master/INSTALL.md)**
Copydir copia la ruta actual a la memoria

```
curl https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/plugins/copypath/copypath.plugin.zsh -o copypath.plugin.zsh && mv copypath.plugin.zsh ~/.oh-my-zsh/custom/plugins
```

**[copyfile](https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/copyfile)**
Copydir copia el contenido de un archivo a la memoria

```
curl https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/plugins/copyfile/copyfile.plugin.zsh -o copyfile.plugin.zsh && mv copyfile.plugin.zsh ~/.oh-my-zsh/custom/plugins
```

* Nota: Es necesario agregar los plugins en el archivo .zshrc
   ```
   plugins=(git
      fast-syntax-highlighting
      zsh-autosuggestions
      copypath
      copyfile
   )
   ```
