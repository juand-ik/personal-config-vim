execute pathogen#infect()
syntax on
filetype plugin indent on
filetype indent on
filetype on
filetype plugin on

" ViM features
set nobackup
set nowritebackup
set noswapfile
set tabstop=4
set shiftwidth=4
set softtabstop=4
set autoindent
set smarttab
set number
set cursorline
set showcmd         " Show (partial) command in status line.
set showmatch       " Show matching brackets.
set ignorecase      " Do case insensitive matching
set smartcase       " Do smart case matching
set incsearch       " Incremental search -- find matches as you type
set hlsearch        " Highlight search
set relativenumber  " And relative line numbers
set t_Co=256

set backspace=indent,eol,start

syntax enable
colorscheme dracula
let g:ctrlp_map = '<c-p>'

let g:EasyMotion_do_mapping = 0
let g:EasyMotion_smartcase = 1
let g:EasyMotion_smartfline = 0
let NERDTreeShowHidden=1

" UltiSnips
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
let g:UltiSnipsSnippetsDir = "~/.vim/bundle/ultisnips/UltiSnips"

nmap s <Plug>(easymotion-s)
nmap s <Plug>(easymotion-s2)

map <Leader>l <Plug>(easymotion-linefoward)
map <Leader>j <Plug>(easymotion-j)
map <Leader>k <Plug>(easymotion-k)
map <Leader>h <Plug>(easymotion-linebackward)

map / <Plug>(easymotion-sn)
map / <Plug>(easymotion-tn)
map n <Plug>(easymotion-next)
map N <Plug>(easymotion-prev)

"highlight ExtraWhitespace ctermbg=red guibg=red
highlight ExtraWhitespace ctermbg=darkgreen guibg=darkgreen
autocmd ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()

" Change tab key behavior
set shiftwidth=4
set tabstop=4

" ---- Laracasts: Vim mastery ----
let mapleader = ','   " The default <Leader> is '\', but ',' seems better

" ------ Mappings ------
" List buffers <= :ls
nmap <Leader>l :ls<cr>
" Open a new tab
nmap <Leader>t :tabnew
" Open a new Horizontal split window
nmap <Leader>s :split
" Open a new Vertically split window
nmap <Leader>v :vsplit

" // NERDTree
" Make NERDTree easier to toggle
nmap <Leader>n :NERDTree
nmap <F6> :NERDTreeToggle<CR>
let NERDTreeHijackNetrw = 0

